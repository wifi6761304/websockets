const WebSocket = require("ws");

// Server erzeugen
const wss = new WebSocket.Server({ port: 8000 });

/**
 * Botschaft an alle clients schicken
 * @param  msg
 */
function broadcast(msg) {
	wss.clients.forEach((client) => {
		if (client.readyState === WebSocket.OPEN) {
			client.send(msg);
		}
	});
}

// Verbindung mit einem Client wurde aufgebaut
wss.on("connection", (ws) => {
	// console.log("New connection from a client.");
	ws.send("Welcome to out simple simulated chat!");
	// message event abfangen
	ws.on("message", (msg) => {
		console.log(`Received: ${msg}`);

		// An den Client eine Message schicken
		broadcast(`Server sends: ${msg}`);
	});

	// Botschaft alle 4 sekunden schicken
	setInterval(() => {
		const now = new Date();
		ws.send(`Message sent on ${now}`);
	}, 4000);
});